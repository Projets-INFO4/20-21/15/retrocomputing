# Project 15: Retrocomputing

## Project Overview
This project is about re-exploring past computer hardware. In particular, our objective is to study the functioning of old processors. As we will not work with actual hardware, we will use the Python language to simulate a processor. We are also making use of the software [Digital](https://github.com/hneemann/Digital) which enables us to easily design combinational circuits. Our objective is to implement the emulated processor as an actual component in Digital.

You can find a more in-depths description of the project on the [AIR Wiki](https://air.imag.fr/index.php/Retrocompute_simulateur).

## Our Team
We are a group of three INFO4 students from Polytech Grenoble:
- Sami ELHADJI TCHIAMBOU
- Corentin HUMBERT
- Mathis MUTEL

## Documentation

Documentation for this project can be found [here](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/15/docs).
