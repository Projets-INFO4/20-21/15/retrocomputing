package de.neemann.digital.plugin;

import de.neemann.digital.core.Node;
import de.neemann.digital.core.NodeException;
import de.neemann.digital.core.ObservableValue;
import de.neemann.digital.core.ObservableValues;
import de.neemann.digital.core.element.Element;
import de.neemann.digital.core.element.ElementAttributes;
import de.neemann.digital.core.element.ElementTypeDescription;
import de.neemann.digital.core.element.Keys;

import static de.neemann.digital.core.element.PinInfo.input;

/**
 * A simple And component
 */
public class MyMUX21 extends Node implements Element {

    /**
     * The description of the new component
     */
    public static final ElementTypeDescription DESCRIPTION
            = new ElementTypeDescription(MyMUX21.class,
            input("a", "MUX21 input a"),
            input("b", "MUX21 input b"),
            input("s", "MUX21 input s")) {

        @Override
        public String getDescription(ElementAttributes elementAttributes) {
            return "A simple MUX21 gate which shows the implementation of a custom component.";
        }

    }
            .addAttribute(Keys.ROTATE)  // allows to rotate the new component
            .addAttribute(Keys.BITS);   // allows to set a bit number to the component


    private final int bits;
    private final ObservableValue out;
    private ObservableValue a;
    private ObservableValue b;
    private ObservableValue s;
    private long outValue;

    /**
     * Creates a component.
     * The constructor is able to access the components attributes and has
     * to create the components output signals, which are instances of the {@link ObservableValue} class.
     * As soon as the constructor is called you have to expect a call to the getOutputs() method.
     *
     * @param attr the attributes which are editable in the components properties dialog
     */
    public MyMUX21(ElementAttributes attr) {
        bits = attr.getBits();
        out = new ObservableValue("out", bits).setDescription("mux21 output");
    }

    /**
     * This method is called if one of the input values has changed.
     * Here you can read the input values of your component.
     * It is not allowed to write to one of the outputs!!!
     */
    @Override
    public void readInputs() {
        long valueA = a.getValue();
        long valueB = b.getValue();
        long valueS = s.getValue();
        outValue = (valueA & ~valueS) | (valueB & valueS);
    }

    /**
     * This method is called if you have to update your output.
     * It is not allowed to read one of the inputs!!!
     */
    @Override
    public void writeOutputs() {
        out.setValue(outValue);
    }

    /**
     * This method is called to register the input signals which are
     * connected to your components inputs. The order is the same as given in
     * the {@link ElementTypeDescription}.
     * You can store the instances, make some checks and so on.
     * IMPORTANT: If it's necessary that your component is called if the input
     * changes, you have to call the addObserverToValue method on that input.
     * If a combinatorial component is implemented you have to add the observer
     * to all inputs. If your component only reacts on a clock signal you only
     * need to add the observer to the clock signal.
     *
     * @param inputs the list of <code>ObservableValue</code>s to use
     * @throws NodeException NodeException
     */
    @Override
    public void setInputs(ObservableValues inputs) throws NodeException {
        a = inputs.get(0).addObserverToValue(this).checkBits(bits, this);
        b = inputs.get(1).addObserverToValue(this).checkBits(bits, this);
        s = inputs.get(2).addObserverToValue(this).checkBits(bits, null);
    }

    /**
     * This method must return the output signals of your component.
     *
     * @return the output signals
     */
    @Override
    public ObservableValues getOutputs() {
        return new ObservableValues(out);
    }
}
