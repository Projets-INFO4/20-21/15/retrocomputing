/*
 * Authors:
 * 	- Sami ELHADJI TCHIAMBOU
 *  - Corentin HUMBERT
 *  - Mathis MUTEL
 * Polytech Grenoble - Université Grenoble Alpes (2021)
 */
package de.neemann.digital.plugin;

import static de.neemann.digital.core.element.PinInfo.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.LinkedList;

import de.neemann.digital.core.Node;
import de.neemann.digital.core.NodeException;
import de.neemann.digital.core.ObservableValue;
import de.neemann.digital.core.ObservableValues;
import de.neemann.digital.core.element.*;

/**
 * MC6809 microprocessor client component. This custom component is an exact
 * replica of the Motorola 6809 microprocessor. At run time, a simple activation
 * of the Vcc pin will attempt to connect to a remote server running a MC6809
 * architecture. The remote processor can be freely controlled with this
 * processor.
 */
public class MC6809 extends Node implements Element {

	public static final ElementTypeDescription DESCRIPTION = new ElementTypeDescription(MC6809.class) {

		@Override
		public String getDescription(ElementAttributes elementAttributes) {
			return "MC6809 TCP-bound microprocessor. When activated (through Vcc pin), this component will attempt to connect to a remote server that can simulate a MC6809 microprocessor.";
		}

		@Override
		public PinDescriptions getInputDescription(ElementAttributes elementAttributes) {
			PinDescription[] pd = new PinDescription[] {
				input("!NMI", "Non Maskable Interrupt. A negative transition on this input requests that a non-maskable interrupt sequence be generated."),
				input("!IRQ", "Interrupt Request. A \"Low\" level input on this pin will initiate an Interrupt Request sequence provided the mask bit (I) in the CC is clear."),
				input("!FIRQ", "Fast-Interrupt Request. A \"Low\" level on this input pin will initate a fast interupt sequence, provided its mask bit (F) in the CC is clear."),
				input("Vcc", "A \"High\" level on this input pin will attempt to connect to the remote the server. A \"Low\" level will close any ungoing connection."),
				input("!DMA!BREQ", "!DMA/!BREQ desc"),
				input("MRDY"),
				input("!Reset"),
				input("EXTAL"),
				input("XTAL"),
				input("!Halt", "A \"Low\" level on this input pin will cause the MPU to stop running at the end of the present instruction and remain halted indefinitely without loss of data"),
				input("Clock") // Local only
			};
			return new PinDescriptions(pd);
		}
	}
	.addAttribute(Keys.ROTATE);	// Enables rotation on the component

	private final int m_bits;
	private LinkedList<ObservableValue> m_outputs;
	private LinkedList<ObservableValue> m_inputs;

	// TCP Control bits
	private static final int INIT_0 = 1;
	private static final int CLOSE_1 = 1;
	private static final int NO_INIT_0 = 0;
	private static final int NO_CLOSE_1 = 0;

	// TCP Message size (number of Digins in a message)
	private static final int TOTAL_PINS = 40;
	private static final int CTRL_BITS = 2;
	private static final int MESSAGE_SIZE = TOTAL_PINS + CTRL_BITS;

	// TCP Server information
	private static final int PORT = 6809;
	private static final String HOST = "localhost";

	private Socket m_socket;
	private BufferedReader m_socketReader;
	private PrintWriter m_socketWriter;

	private char[] m_digins;

	public enum TCPState {
		Disconnected, Connected, Receive, Send
	}

	private TCPState m_state;

	// Pin related information
	private int ADDRESS_BITS = 16;
	private int DATA_BITS = 8;

	private boolean m_lastVcc = false;
	private boolean m_lastClock = false;
	private boolean m_writeMode = false;

	public MC6809(ElementAttributes attr) {
		m_state = TCPState.Disconnected;
		m_bits = attr.getBits();
		setOutputs(m_bits);
	}

	public String getPinStates() {
		int[] pinStates = new int[TOTAL_PINS];
		pinStates[UniversalPins.DP_Vss] = 0;
		pinStates[UniversalPins.DP_notNMI] = (int) m_inputs.get(DigitalPins.in_notNMI).getValue();
		pinStates[UniversalPins.DP_notIRQ] = (int) m_inputs.get(DigitalPins.in_notIRQ).getValue();
		pinStates[UniversalPins.DP_notFIRQ] = (int) m_inputs.get(DigitalPins.in_notFIRQ).getValue();
		pinStates[UniversalPins.DP_BS] = (int) m_outputs.get(DigitalPins.out_BS).getValue();
		pinStates[UniversalPins.DP_BA] = (int) m_outputs.get(DigitalPins.out_BA).getValue();
		pinStates[UniversalPins.DP_Vcc] = (int) m_outputs.get(DigitalPins.in_Vcc).getValue();
		
		for (int i = 0; i < ADDRESS_BITS; i++)
			pinStates[UniversalPins.DP_A0 + i] = (int) m_outputs.get(DigitalPins.out_A0 + i).getValue();
		
		for (int i = 0; i < DATA_BITS; i++)
			pinStates[UniversalPins.DP_D0 + i] = (int) m_inputs.get(DigitalPins.in_D0 + i).getValue();
		
		pinStates[UniversalPins.DP_RnotW] = (int) m_outputs.get(DigitalPins.out_RnotW).getValue();
		pinStates[UniversalPins.DP_notDMAnotBREQ] = (int) m_inputs.get(DigitalPins.in_notDMAnotBREQ).getValue();
		pinStates[UniversalPins.DP_E] = (int) m_outputs.get(DigitalPins.out_E).getValue();
		pinStates[UniversalPins.DP_Q] = (int) m_outputs.get(DigitalPins.out_Q).getValue();
		pinStates[UniversalPins.DP_MRDY] = (int) m_inputs.get(DigitalPins.in_MRDY).getValue();
		pinStates[UniversalPins.DP_notReset] = (int) m_inputs.get(DigitalPins.in_notReset).getValue();
		pinStates[UniversalPins.DP_EXTAL] = (int) m_inputs.get(DigitalPins.in_EXTAL).getValue();
		pinStates[UniversalPins.DP_XTAL] = (int) m_inputs.get(DigitalPins.in_XTAL).getValue();
		pinStates[UniversalPins.DP_notHALT] = (int) m_inputs.get(DigitalPins.in_notHalt).getValue();
		
		String msg = "";
		for (int i : pinStates) {
			msg += i + "";
		}
		return msg;
	}

	/**
	 * The element has to provide all the output values of the whole component.
	 *
	 * @return the outputs available
	 */
	@Override
	public ObservableValues getOutputs() {
		ObservableValue[] array = new ObservableValue[m_outputs.size()];
		array = m_outputs.toArray(array);
		return new ObservableValues(array);
	}

	/**
	 * And the element also receives all the required input values.
	 *
	 * @param inputs the inputs
	 * @throws NodeException NodeException
	 */
	@Override
	public void setInputs(ObservableValues inputs) throws NodeException {
		System.out.println("Initializing inputs (" + inputs.size() + ")");

		m_inputs = new LinkedList<>();
		for (ObservableValue input : inputs) {
			m_inputs.add(input.addObserverToValue(this).checkBits(m_bits, this));
		}
	}

	public void setOutputs(int bits) {
		System.out.println("Initializing outputs");
		m_outputs = new LinkedList<ObservableValue>();
		
		m_outputs.add(new ObservableValue("BS", bits));
		m_outputs.add(new ObservableValue("BA", bits));
		
		for (int i = 0; i < ADDRESS_BITS; i++) {
			m_outputs.add(new ObservableValue("A" + i, bits));
		}
		
		for (int i = 0; i < DATA_BITS; i++) {
			ObservableValue ov = new ObservableValue("D" + i, bits);
			ov.setBidirectional();
			m_outputs.add(ov);
		}
		
		m_outputs.add(new ObservableValue("R/!W", bits));
		m_outputs.add(new ObservableValue("E", bits));
		m_outputs.add(new ObservableValue("Q", bits));
	}

	public void initConnection() {
		try {
			// Simply try to connect to the server
			m_socket = new Socket(HOST, PORT);
			m_socketWriter = new PrintWriter(m_socket.getOutputStream());
			m_socketReader = new BufferedReader(new InputStreamReader(m_socket.getInputStream()));
		} catch (IOException e) {
			System.out.println("Could not connect to server... Server down?");
			e.printStackTrace();
		}
	}

	public void closeConnection() {
		try {
			send(NO_INIT_0, CLOSE_1);
			m_socketWriter.close();
			m_socketReader.close();
			m_socket.close();
			System.out.println("Client closed the connection");
		} catch (IOException e) {
			System.out.println("Not connected to the server");
			e.printStackTrace();
		}
	}

	@Override
	public void readInputs() throws NodeException {
		reactToVcc();
		reactToClock();
	}

	private void reactToVcc() {
		ObservableValue vcc = m_inputs.get(DigitalPins.in_Vcc);
		if (vcc == null) {
			System.out.println("Error while trying to get Vcc in reactToVcc");
		}

		boolean newVcc = vcc.getBool();

		if (m_lastVcc != newVcc) {
			if (newVcc) {
				// Connect to server
				initConnection();
				m_state = TCPState.Connected;
			} else {
				// Disconnect from server
				closeConnection();
				m_state = TCPState.Disconnected;
			}
		}
		m_lastVcc = newVcc;
	}

	private void reactToClock() {
		ObservableValue clock = m_inputs.get(DigitalPins.in_Clock);
		if (clock == null) {
			System.out.println("Error while trying to get Clock in reactToClock");
		}

		boolean newClock = clock.getBool();

		if (m_lastClock != newClock) {
			if (newClock) { // Rising edge
				if (m_state == TCPState.Connected || m_state == TCPState.Send) {
					m_state = TCPState.Receive;
					receive();
				} else if (m_state == TCPState.Receive) {
					m_state = TCPState.Send;
				}
			}
		}
		m_lastClock = newClock;
	}

	public void receive() {
		if (m_state != TCPState.Receive) {
			System.out.println("Can not receive out of receive state");
			System.exit(-1);
		}
		
		// We start by receiving the data sent by the server
		m_digins = new char[MESSAGE_SIZE];

		try {
			int read = m_socketReader.read(m_digins, 0, MESSAGE_SIZE);
			if (read != MESSAGE_SIZE) {
				System.out.println("Unexpected number of digins in message. \n\tReceived: " + read + "\n\tExpected: " + MESSAGE_SIZE);
				System.exit(-1);
			}

			System.out.println("Received " + String.valueOf(m_digins));

		} catch (IOException e) {
			System.out.println("Server error encountered in handleReceive()");
			e.printStackTrace();
		}
	}

	private void processMessage(char[] message) {
		assert (message != null && message.length == MESSAGE_SIZE);
		
		char[] ctrlBits = new char[CTRL_BITS];
		for (int i = 0; i < CTRL_BITS; i++)
			ctrlBits[i] = message[i];
		
		// If the server sent a close connection request
		if (ctrlBits[0] == NO_INIT_0 && ctrlBits[1] == CLOSE_1) {
			// TODO: close connection if the server has stopped
		}
		
		char[] digins = new char[TOTAL_PINS];
		System.arraycopy(message, CTRL_BITS, digins, 0, TOTAL_PINS);
		
		setOutPin(DigitalPins.out_BS, digins[UniversalPins.DP_BS]);
		setOutPin(DigitalPins.out_BA, digins[UniversalPins.DP_BA]);
		
		// We reverse the address bits because they were inverted when sent
		char[] reversedAddr = new char[ADDRESS_BITS];
		System.arraycopy(digins, UniversalPins.DP_A0, reversedAddr, 0, ADDRESS_BITS);
		reversedAddr = reverseArray(reversedAddr);

		int j = 0;
		for (int i = DigitalPins.out_A0; i <= DigitalPins.out_A15; i++) {
			setOutPin(i, reversedAddr[j]);
			j++;
		}

		char[] reversedData = new char[DATA_BITS];
		System.arraycopy(digins, UniversalPins.DP_D0, reversedData, 0, DATA_BITS);
			
		m_writeMode = digins[UniversalPins.DP_RnotW] == '0';

		j = 0;
		for (int i = DigitalPins.out_D0; i <= DigitalPins.out_D7; i++) {
			if (m_writeMode) {
				// If the mode is "Write", we set the output pins appropriately
				setOutPin(i, reversedData[j]);
			} else {
				// If the mode is "Read", we set the output pins to high Z because want to use them as inputs
				m_outputs.get(i).setToHighZ();
			}
			j++;
		}

		setOutPin(DigitalPins.out_RnotW, digins[UniversalPins.DP_RnotW]);
		setOutPin(DigitalPins.out_E, digins[UniversalPins.DP_E]);
		setOutPin(DigitalPins.out_Q, digins[UniversalPins.DP_Q]);
	}
	
	private void setOutPin(int digitalPin, char bit) {
		ObservableValue ov = m_outputs.get(digitalPin);
		ov.setBool(bit == '1');
	}

	public void send(int cb0, int cb1) {
		String msg = cb0 + "" + cb1;
		msg += this.getPinStates();
		System.out.println("Sending  " + msg);
		this.m_socketWriter.print(msg);
		this.m_socketWriter.flush();
	}

	@Override
	public void writeOutputs() throws NodeException {
		if (!m_lastClock)
			return;
		
		if (m_state == TCPState.Receive) {
			System.out.println("Processing digins...");
			processMessage(m_digins);
		} else if (m_state == TCPState.Send) {
			if (m_writeMode)
				return;
			
			send(NO_INIT_0, NO_INIT_0);
		}
	}

	// Reverses the characters in the given array
	private static char[] reverseArray(char[] arr) {
		char[] niu = new char[arr.length];
		int i = arr.length - 1;
		for (char c : arr) {
			niu[i] = c;
			i--;
		}
		return niu;
	}

	// The order of input and output pins in their respective list m_inputs and m_outputs
	private class DigitalPins {
		/* Input pins */
		public static final int in_notNMI = 0;
		public static final int in_notIRQ = 1;
		public static final int in_notFIRQ = 2;
		public static final int in_Vcc = 3;
		public static final int in_notDMAnotBREQ = 4;
		public static final int in_MRDY = 5;
		public static final int in_notReset = 6;
		public static final int in_EXTAL = 7;
		public static final int in_XTAL = 8;
		public static final int in_notHalt = 9;
		public static final int in_Clock = 10; // local
		public static final int in_D0 = 11;
		public static final int in_D7 = 18;

		/* Output pins */
		public static final int out_BS = 0;
		public static final int out_BA = 1;
		public static final int out_A0 = 2;
		public static final int out_A15 = 17;
		public static final int out_D0 = 18;
		public static final int out_D7 = 25;
		public static final int out_RnotW = 26;
		public static final int out_E = 27;
		public static final int out_Q = 28;
	}
	
	// The order of pins as defined in the official data sheet for the MC6809 : http://atjs.mbnet.fi/mc6809/Information/6809pin.txt
	// More information about the MC6809 can be found here: http://atjs.mbnet.fi/mc6809/#Infohttp://atjs.mbnet.fi/mc6809/#Info
	private class UniversalPins {
		public static final int DP_Vss = 0;
		public static final int DP_notNMI = 1;
		public static final int DP_notIRQ = 2;
		public static final int DP_notFIRQ = 3;
		public static final int DP_BS = 4;
		public static final int DP_BA = 5;
		public static final int DP_Vcc = 6;
		public static final int DP_A0 = 7;
		public static final int DP_A1 = 8;
		public static final int DP_A2 = 9;
		public static final int DP_A3 = 10;
		public static final int DP_A4 = 11;
		public static final int DP_A5 = 12;
		public static final int DP_A6 = 13;
		public static final int DP_A7 = 14;
		public static final int DP_A8 = 15;
		public static final int DP_A9 = 16;
		public static final int DP_A10 = 17;
		public static final int DP_A11 = 18;
		public static final int DP_A12 = 19;
		public static final int DP_A13 = 20;
		public static final int DP_A14 = 21;
		public static final int DP_A15 = 22;
		public static final int DP_D0 = 23;
		public static final int DP_D1 = 24;
		public static final int DP_D2 = 25;
		public static final int DP_D3 = 26;
		public static final int DP_D4 = 27;
		public static final int DP_D5 = 28;
		public static final int DP_D6 = 29;
		public static final int DP_D7 = 30;
		public static final int DP_RnotW = 31;
		public static final int DP_notDMAnotBREQ = 32;
		public static final int DP_E = 33;
		public static final int DP_Q = 34;
		public static final int DP_MRDY = 35;
		public static final int DP_notReset = 36;
		public static final int DP_EXTAL = 37;
		public static final int DP_XTAL = 38;
		public static final int DP_notHALT = 39;
	}
}
