U
    \?!`�  �                   @   s"   d Z ddlmZ G dd� d�ZdS )a9  
    MC6809 - 6809 CPU emulator in Python
    =======================================

    6809 is Big-Endian

    Links:
        http://dragondata.worldofdragon.org/Publications/inside-dragon.htm
        http://www.burgins.com/m6809.html
        http://koti.mbnet.fi/~atjs/mc6809/

    :copyleft: 2013-2015 by the MC6809 team, see AUTHORS for more details.
    :license: GNU GPL v3 or above, see LICENSE for more details.

    Based on:
        * ApplyPy by James Tauber (MIT license)
        * XRoar emulator by Ciaran Anscomb (GPL license)
    more info, see README
�    )�opcodec                   @   s�   e Zd Zed�dd� �Zed�dd� �ZdZdd	� Zd
d� Zdd� Z	ed�dd� �Z
ed�dd� �Zed�dd� �Zed�dd� �Zed�dd� �ZdS )�InterruptMixin�<   c                 C   s   dS )a(  
        This instruction ANDs an immediate byte with the condition code register
        which may clear the interrupt mask bits I and F, stacks the entire
        machine state on the hardware stack and then looks for an interrupt.
        When a non-masked interrupt occurs, no further machine state information
        need be saved before vectoring to the interrupt handling routine. This
        instruction replaced the MC6800 CLI WAI sequence, but does not place the
        buses in a high-impedance state. A FIRQ (fast interrupt request) may
        enter its interrupt handler with its entire machine state saved. The RTI
        (return from interrupt) instruction will automatically return the entire
        machine state after testing the E (entire) bit of the recovered
        condition code register.

        The following immediate values will have the following results: FF =
        enable neither EF = enable IRQ BF = enable FIRQ AF = enable both

        source code forms: CWAI #$XX E F H I N Z V C

        CC bits "HNZVC": ddddd
        N� )�selfr   �mr   r   �Y/home/coko/Workspace/INFO4/S8/RetroComputing/MC6809/MC6809/components/mc6809_interrupt.py�instruction_CWAI   s    zInterruptMixin.instruction_CWAI�>   c                 C   s   t d|d�d���dS )z�
        Build the ASSIST09 vector table and setup monitor defaults, then invoke
        the monitor startup routine.

        source code forms:

        CC bits "HNZVC": *****
        �$�xz RESETN��NotImplementedError�r   r   r   r   r   �instruction_RESET:   s    z InterruptMixin.instruction_RESETFc                 C   sJ   | j r| jdkrd S | jr$| ��  n| ��  | j�| j�}| j�	|� d S )N�   )
�irq_enabled�I�E�push_irq_registers�push_firq_registers�memory�	read_word�
IRQ_VECTOR�program_counter�set)r   �ear   r   r   �irqM   s    
zInterruptMixin.irqc                 C   s�   |  j d7  _ | �| j| jj� | �| j| jj� | �| j| jj� | �| j| jj� | �| j| j	j� | �| j| j
j� | �| j| jj� | �| j| �� � dS )zH
        push PC, U, Y, X, DP, B, A, CC on System stack pointer
        r   N)�cycles�	push_word�system_stack_pointerr   �value�user_stack_pointer�index_y�index_x�	push_byte�direct_page�accu_b�accu_a�get_cc_value�r   r   r   r   r   _   s    z!InterruptMixin.push_irq_registersc                 C   s6   |  j d7  _ | �| j| jj� | �| j| �� � dS )z^
        FIRQ - Fast Interrupt Request
        push PC and CC on System stack pointer
        r   N)r   r   r    r   r!   r%   r)   r*   r   r   r   r   m   s    z"InterruptMixin.push_firq_registers�;   c                 C   s�   | � | j�}| �|� | jr�| j�| � | j�� | j�| � | j�� | j�| � | j�� | j�| �	| j�� | j
�| �	| j�� | j�| �	| j�� | j�| �	| j�� dS )a�  
        The saved machine state is recovered from the hardware stack and control
        is returned to the interrupted program. If the recovered E (entire) bit
        is clear, it indicates that only a subset of the machine state was saved
        (return address and condition codes) and only that subset is recovered.

        source code forms: RTI

        CC bits "HNZVC": -----
        N)Z	pull_byter    �set_ccr   r(   r   r'   r&   r$   Z	pull_wordr#   r"   r   )r   r   �ccr   r   r   �instruction_RTIv   s0    

�
�
�
�
�
�
�zInterruptMixin.instruction_RTI�?   c                 C   s   t d|d�d���dS )ac  
        All of the processor registers are pushed onto the hardware stack (with
        the exception of the hardware stack pointer itself), and control is
        transferred through the software interrupt vector. Both the normal and
        fast interrupts are masked (disabled).

        source code forms: SWI

        CC bits "HNZVC": -----
        r   r   z SWINr   r   r   r   r   �instruction_SWI�   s    zInterruptMixin.instruction_SWIi?  c                 C   s   t d|d�d���dS )a�  
        All of the processor registers are pushed onto the hardware stack (with
        the exception of the hardware stack pointer itself), and control is
        transferred through the software interrupt 2 vector. This interrupt is
        available to the end user and must not be used in packaged software.
        This interrupt does not mask (disable) the normal and fast interrupts.

        source code forms: SWI2

        CC bits "HNZVC": -----
        r   r   z SWI2Nr   �r   r   r   r   r   r   r   �instruction_SWI2�   s    zInterruptMixin.instruction_SWI2i?  c                 C   s   t d|d�d���dS )ar  
        All of the processor registers are pushed onto the hardware stack (with
        the exception of the hardware stack pointer itself), and control is
        transferred through the software interrupt 3 vector. This interrupt does
        not mask (disable) the normal and fast interrupts.

        source code forms: SWI3

        CC bits "HNZVC": -----
        r   r   z SWI3Nr   r1   r   r   r   �instruction_SWI3�   s    zInterruptMixin.instruction_SWI3�   c                 C   s   t d|d�d���dS )z�
        FAST SYNC WAIT FOR DATA Interrupt! LDA DISC DATA FROM DISC AND CLEAR
        INTERRUPT STA ,X+ PUT IN BUFFER DECB COUNT IT, DONE? BNE FAST GO AGAIN
        IF NOT.

        source code forms: SYNC

        CC bits "HNZVC": -----
        r   r   z SYNCNr   r   r   r   r   �instruction_SYNC�   s    zInterruptMixin.instruction_SYNCN)�__name__�
__module__�__qualname__r   r	   r   r   r   r   r   r.   r0   r2   r3   r5   r   r   r   r   r      s@   �
�
	�
&�
�
�
�r   N)�__doc__�.MC6809.components.cpu_utils.instruction_callerr   r   r   r   r   r   �<module>   s   