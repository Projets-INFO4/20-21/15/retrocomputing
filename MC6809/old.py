#!/usr/bin/env python

"""
    MC6809 Remote Server
    ~~~~~~~~~~~~~~
"""

from MC6809.components.cpu6809 import CPU

from digitalCore import DigitalCore
from digitalMemory import DigitalMemory
from MC6809.core.configs import BaseConfig

# TCP Configuration
TCP_HOST = 'localhost'
TCP_PORT = 6809

DEBUG_MODE = True

CFG_DICT = {
    "verbosity": None,
    "trace": None,
}


class Config(BaseConfig):
    RAM_START = 0x0000
    RAM_END = 0x7FFF

    ROM_START = 0x8000
    ROM_END = 0xFFFF


class MC6809Server:
    def __init__(self, pc_start=0x100, pc_end=None):
        self.digital_core = DigitalCore(TCP_HOST, TCP_PORT)
        cfg = Config(CFG_DICT)
        memory = DigitalMemory(cfg, self.digital_core)
        self.cpu = CPU(memory, cfg)
        self.start_address = pc_start
        self.end_address = pc_end

    def cpu_test_run(self, start, end, mem):
        assert isinstance(mem, bytearray), "given mem is not a bytearray!"

        print("memory load at $%x: %s", start,
              ", ".join("$%x" % i for i in mem)
              )
        self.cpu.memory.load(start, mem)
        if end is None:
            end = start + len(mem)
        self.cpu.test_run(start, end)

    def run(self):
        self.cpu.test_run(start=self.start_address, end=self.end_address)

        print("=== 16 bit registers ===")
        print("X:", self.cpu.index_x.value)
        print("Y:", self.cpu.index_y.value)
        print("U:", self.cpu.user_stack_pointer)
        print("S:", self.cpu.system_stack_pointer)
        print("PC:", self.cpu.program_counter.value)
        print("D:", self.cpu.accu_d)

        print("=== 8 bit registers ====")
        print("A:", self.cpu.accu_a.value)
        print("B:", self.cpu.accu_b.value)
        print("DP:", self.cpu.direct_page.value)
        print("CC:", self.cpu.cc_register.value)

    def run_code(self, data):
        data_address = 0x1000  # position of the test data
        self.cpu.memory.load(data_address, data)  # write test data into RAM
        # self.cpu.index_x.set(data_address + len(data))  # end address
        self.cpu.index_x.set(0xffff)
        addr_hi, addr_lo = divmod(data_address, 0x100)  # start address

        self.cpu_test_run(start=0x0100, end=None, mem=bytearray([
            # A = atoi(*(0x1000)) + atoi(*(0x1001)):
            0xB6, 0x10, 0x00,  # LDA       (extended)      0x1000
            0x80, 0x30,  # SUBA      (immediate)     0x30 (48 in base 10)
            0xF6, 0x10, 0x01,  # LDB       (extended)      0x1001
            0xC0, 0x30,  # SUBB      (immediate)     0x30 (48 in base 10)
            0xF7, 0x10, 0x02,  # STB       (extended)      0x1002
            0xBB, 0x10, 0x02,  # ADDA      (extended)      0x1002
            0xB7, 0x10, 0x03  # STA       (extended)      0x1003
        ]))

        self.cpu_test_run(start=0x0100, end=None, mem=bytearray([
            # B = atoi(*(0x1000)):
            0xF6, 0x10, 0x00,   # LDB       (extended)      0x1000
            0xC0, 0x30,         # SUBB      (immediate)     0x30 (48 in base 10)
            0xF7, 0x10, 0x01,   # STB       (extended)      0x1001

            # B = 0x34 ('4')
            # B = B - 0x30 (48) -> B = 0x04
            # return B
        ]))

        self.cpu_test_run(start=0x0100, end=None, mem=bytearray([
            # A = atoi(*(0x200)):
            0xB6, 0x02, 0x00,  # LDA       (extended)      0x0200
            0x80, 0x30,        # SUBA      (immediate)     0x30 (48 in base 10)
            0xB7, 0x02, 0x01,  # STA       (extended)      0x0201

            # B = 0x34 ('4')
            # B = B - 0x30 (48) -> B = 0x04
            # return B
        ]))


def main():
    mc6809 = MC6809Server()
    mc6809.run()

    # data = bytes("43", encoding="ASCII")


if __name__ == '__main__':
    main()
