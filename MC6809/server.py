import socket
from _thread import *
from digitalCore import DigitalCore

# TCP Configuration
TCP_HOST = 'localhost'
TCP_PORT = 6809
MAX_CLIENTS = 5
SERVER_NAME = "[Py6809-S]"


def multi_threaded_client(connection, client_id):
    # instantiate new digitalCore
    dc = DigitalCore(connection=connection, client_id=client_id, pc_start=0x100, pc_end=None)
    dc.run()
    connection.close()


server_socket = socket.socket()
host = TCP_HOST
port = TCP_PORT
threads = 0
try:
    server_socket.bind((host, port))
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
except socket.error as e:
    print(str(e))

print(f"{SERVER_NAME} Server ready, waiting for clients to connect...")
server_socket.listen(MAX_CLIENTS)

while True:
    client, addr = server_socket.accept()
    print(f"{SERVER_NAME} {addr[0]}:{addr[1]} successfully connected!")
    start_new_thread(multi_threaded_client, (client, threads))
    print(f'Thread number: {threads}')
    threads += 1

server_socket.close()
