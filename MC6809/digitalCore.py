#!/usr/bin/env python

"""
    MC6809 Digital Core
    ~~~~~~~~~~~~~~
"""

import sys
from enum import Enum

from MC6809.components.cpu6809 import CPU
from MC6809.core.configs import BaseConfig

from digitalMemory import DigitalMemory

TCP_BUFFER_SIZE = 42  # Normally 1024, but we want fast response

CTRL_BITS = 2
TOTAL_PINS = 40


# Converts the given word into an array of binary values
def word_to_bit_array(word: int) -> [bool]:
    bit_str = '{0:16b}'.format(word)
    bit_arr = [False] * 16
    for i in range(16):
        if bit_str[i] == ' ' or bit_str[i] == '0':
            continue
        elif bit_str[i] == '1':
            bit_arr[i] = True
        else:
            sys.exit("illegal value found in word_to_bit_array()")
    return bit_arr


# Converts the given byte into an array of binary values
def byte_to_bit_array(word: int) -> [bool]:
    bit_str = '{0:8b}'.format(word)
    bit_arr = [False] * 8
    for i in range(8):
        if bit_str[i] == ' ' or bit_str[i] == '0':
            continue
        elif bit_str[i] == '1':
            bit_arr[i] = True
        else:
            sys.exit("illegal value found in byte_to_bit_array()")
    return bit_arr


# Enumeration containing all 40 Digital pins
# The actual list of pins comes from http://atjs.mbnet.fi/mc6809/Information/6809pin.txt
class DigitalPin(Enum):
    DP_Vss = 0
    DP_notNMI = 1
    DP_notIRQ = 2
    DP_notFIRQ = 3
    DP_BS = 4
    DP_BA = 5
    DP_Vcc = 6
    DP_A0 = 7
    DP_A1 = 8
    DP_A2 = 9
    DP_A3 = 10
    DP_A4 = 11
    DP_A5 = 12
    DP_A6 = 13
    DP_A7 = 14
    DP_A8 = 15
    DP_A9 = 16
    DP_A10 = 17
    DP_A11 = 18
    DP_A12 = 19
    DP_A13 = 20
    DP_A14 = 21
    DP_A15 = 22
    DP_D0 = 23
    DP_D1 = 24
    DP_D2 = 25
    DP_D3 = 26
    DP_D4 = 27
    DP_D5 = 28
    DP_D6 = 29
    DP_D7 = 30
    DP_RnotW = 31
    DP_notDMAnotBREQ = 32
    DP_E = 33
    DP_Q = 34
    DP_MRDY = 35
    DP_notReset = 36
    DP_EXTAL = 37
    DP_XTAL = 38
    DP_notHALT = 39


CFG_DICT = {
    "verbosity": None,
    "trace": None,
}


class Config(BaseConfig):
    RAM_START = 0x0000
    RAM_END = 0x7FFF

    ROM_START = 0x8000
    ROM_END = 0xFFFF


class DigitalCore:
    def __init__(self, connection, client_id: int, pc_start: hex, pc_end: hex):
        self.conn = connection
        self.client_id = client_id
        cfg = Config(CFG_DICT)
        memory = DigitalMemory(cfg, self)
        self.cpu = CPU(memory, cfg)
        self.start_address = pc_start
        self.end_address = pc_end
        self.digital_pins = [False] * TOTAL_PINS

    def run(self):
        self.cpu.test_run(start=self.start_address, end=self.end_address)

    def get_name(self):
        return "[Py6809-t" + str(self.client_id) + "]"

    def process_ctrl_bits(self, bits):
        assert (len(bits) == CTRL_BITS)
        if bits[0] == '0' and bits[1] == '1':
            print(self.get_name(), "Remote client has disconnected")
            self.conn.close()
            exit(0)
        # TODO: process ctrl bits (end connection)

    # Send packet on TCP socket
    def send_digins(self) -> None:
        msg = self.digins_to_string()
        self.conn.send(msg.encode())
        # print("sending", msg)

    # Wait for packet to arrive on TCP socket
    def receive_digins(self) -> None:
        digins = self.conn.recv(TCP_BUFFER_SIZE).decode()
        # print("received", digins)
        self.string_to_digins(digins)

    def await_read_byte(self, address: int) -> int:
        print(f"{self.get_name()} Reading at {hex(address)}")

        # Set the Read / !Write bit to 1 since we want to read from memory
        self.digital_pins[DigitalPin.DP_RnotW.value] = True

        # Convert the integer address into a binary number and update the address pins (DP_A0, ..., DP_A15) accordingly
        address_bits = word_to_bit_array(address)
        for i in range(DigitalPin.DP_A0.value, DigitalPin.DP_A15.value + 1):
            self.digital_pins[i] = address_bits[i - DigitalPin.DP_A0.value]

        # TODO: send TCP Packet to Digital with updated pins
        self.send_digins()

        # TODO: receive TP Packet from Digital with data and return it
        self.receive_digins()

        if not self.digital_pins[DigitalPin.DP_notReset.value]:
            self.cpu.reset()
            return -1

        # Create a data array containing the byte corresponding to DP_D0, ..., DP_D7
        data = [False] * 8
        for i in range(DigitalPin.DP_D0.value, DigitalPin.DP_D7.value + 1):
            data[i - DigitalPin.DP_D0.value] = self.digital_pins[i]

        data.reverse()

        # Convert the byte array into an integer
        byte = 0
        for i in range(len(data)):
            if data[i]:
                byte += pow(2, i)

        print(f"{self.get_name()} Received {hex(byte)} from client")
        return byte

    def await_write_byte(self, address: int, value: int) -> None:
        print(f"{self.get_name()} Writing {hex(value)} (dec: {value}) at {hex(address)}")

        # Set the Read / !Write bit to 0 since we want to write into memory
        self.digital_pins[DigitalPin.DP_RnotW.value] = False

        # Convert the integer address into a binary number and update the pins accordingly
        address_bits = word_to_bit_array(address)
        for i in range(DigitalPin.DP_A0.value, DigitalPin.DP_A15.value + 1):
            self.digital_pins[i] = address_bits[i - DigitalPin.DP_A0.value]

        # Convert the integer value into a binary number and update the data pins (DP_D0, ..., DP_D7) accordingly
        value_bits = byte_to_bit_array(value)
        for i in range(DigitalPin.DP_D0.value, DigitalPin.DP_D7.value + 1):
            self.digital_pins[i] = value_bits[i - DigitalPin.DP_D0.value]

        # TODO: send TCP Packet to Digital with updated pins
        self.send_digins()

    # Parses a string of binaries received from Digital and sets the pin values accordingly
    def string_to_digins(self, message: str) -> None:
        ctrl_bits = message[:CTRL_BITS]
        pin_bits = message[CTRL_BITS:]

        self.process_ctrl_bits(ctrl_bits)

        for i in range(TOTAL_PINS):
            bit = pin_bits[i]
            if bit == '1':
                self.digital_pins[i] = True
            elif bit == '0':
                self.digital_pins[i] = False
            else:
                sys.exit(f"{self.get_name()} Illegal value '{bit}' found when parsing digin in string_to_digins()")

    # Encode digins into a string of binaries ready to be sent over to Digital (includes control bits)
    def digins_to_string(self) -> str:
        msg = "00"  # TODO: Initialize control bits correctly

        for dp in self.digital_pins:
            msg += '1' if dp else '0'

        return msg
